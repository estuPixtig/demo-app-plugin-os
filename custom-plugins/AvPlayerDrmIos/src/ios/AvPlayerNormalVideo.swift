import UIKit
import MediaPlayer
import AVKit
import AVFoundation

@objc(AvPlayerDrmIos) class AvPlayerDrmIos : CDVPlugin{
     // MARK: Properties
    var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
    var playerViewController:AVPlayerViewController!

     override func pluginInitialize() {
         playerViewController = AVPlayerViewController()
     }

      @objc(play:)
         func play(_ command: CDVInvokedUrlCommand){
            var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
             let url = (command.arguments[0] as? NSObject)?.value(forKey: "url") as? String
             if((url) != nil){
                 let videoURL = URL(string: url!)
                 let player = AVPlayer(url: videoURL!)
                 self.present(player: player)
                 self.commandDelegate!.send(pluginResult,
                                                  callbackId: command.callbackId)
             }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR,
                                                               messageAs: "Something wrong")
             }
         }

         private func present(player:AVPlayer){

             playerViewController.player = player

             DispatchQueue.main.async {
                 self.viewController.present(self.playerViewController, animated: true) {
                     self.playerViewController.player!.play()
                 }

             }

         }

         deinit {
             self.playerViewController = nil
         }

}
