import UIKit
import MediaPlayer
import AVKit
import AVFoundation

fileprivate let certificateURL = "https://s3.amazonaws.com/ott-content/gdvcas-develop/assets/fps_certificate.der"
fileprivate let licenseURL = "https://multidrm.vsaas.verimatrixcloud.net/fairplay"


@objc(AvPlayerDrmIos) class AvPlayerDrmIos : CDVPlugin{
     // MARK: Properties
    var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
    var player: AVPlayer!
    var certificate: Data?
    var playerViewController:AVPlayerViewController!
    var jwt: String?
    var streamURL: String?
     override func pluginInitialize() {
        playerViewController = AVPlayerViewController()
     }

      @objc(play:)
         func play(_ command: CDVInvokedUrlCommand){
            var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
             self.streamURL = (command.arguments[0] as? NSObject)?.value(forKey: "url") as? String
             self.jwt = (command.arguments[0] as? NSObject)?.value(forKey: "token") as? String
             print("📜 token", #function, self.jwt)
             //Start With HLS DRM FAIRPLAY
             if((self.streamURL) != nil){
                print("📜", #function, "On ViewController.")
                    self.requestApplicationCertificate { (cer, error) in
                    print("📜", #function, "GIn requestingCertificate.")
                      if error == nil && cer != nil {
                      print("📜 certificate", #function, cer! as NSData)
                        self.certificate = cer
                        if let url = URL(string: self.streamURL ?? "") {
                          DispatchQueue.main.async {
                            //2. Create AVPlayer object
                            let asset = AVURLAsset(url: url)
                            let queue = DispatchQueue(label: "LicenseGetQueue")
                            asset.resourceLoader.setDelegate(self, queue: queue)
                            let playerItem = AVPlayerItem(asset: asset)
                            self.player = AVPlayer(playerItem: playerItem)
                            //5. Play Video
                            self.present(player: self.player)
                          }
                        }
                      } else {
                        print("📜", #function, "Unable to fetch the certificate.")
                      }
                    }
             }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR,
                                                               messageAs: "Something wrong")
             }
         }

         func requestApplicationCertificate(with completion: @escaping (Data? , Error?) -> ()) {
             // This function gets the FairPlay application certificate, expected in DER format, from the
             // configured URL. In general, the logic to obtain the certificate is up to the playback app
             // implementers. Implementers should use their own certificate, received from Apple upon request.
             print("📜", #function, "In requestingCertificate Function.")
             let configuration = URLSessionConfiguration.default
             let session = URLSession(configuration: configuration)
             guard let url = URL(string: certificateURL) else {return}
             session.dataTask(with: url, completionHandler: { data, response, error in
               completion(data, error)
             }).resume()
             print("📜", #function, "Certificate granted.")
           }

           func requestContentKeyAndLeaseExpiryfromKeyServerModule(withRequestBytes requestBytes: Data?, completion: @escaping (Data?, Error?) -> ()) {
             print("📜 LICENSE", #function, "Geting License")
             let ckcURL = URL(string: licenseURL)!
             var request = URLRequest(url: ckcURL)
             request.httpMethod = "POST"
             request.setValue("application/json", forHTTPHeaderField: "Content-Type")
             request.setValue("\(jwt)", forHTTPHeaderField: "Authorization")
             var requestBodyComponent = URLComponents()
             requestBodyComponent.queryItems = [URLQueryItem(name: "spc", value: requestBytes?.base64EncodedString())]
             request.httpBody = requestBodyComponent.query?.data(using: .utf8)
             let session = URLSession(configuration: URLSessionConfiguration.default)
             session.dataTask(with: request) { data, response, error in
               if let data = data, var responseString = String(data: data, encoding: .utf8) {
                 responseString = responseString.replacingOccurrences(of: "<ckc>", with: "").replacingOccurrences(of: "</ckc>", with: "")
                 let ckcData = Data(base64Encoded: responseString)
                 print("📜 ckcData", #function, ckcData)
                 completion(ckcData, error)
               } else {
                  print("📜 LICENSE", #function, "ERROR Geting License")
                 completion(nil, error)
               }
             }.resume()
           }

      private func present(player:AVPlayer){

          playerViewController.player = player

          DispatchQueue.main.async {
              self.viewController.present(self.playerViewController, animated: true) {
                  self.playerViewController.player!.play()
              }

          }

      }

      deinit {
          self.playerViewController = nil
      }

}

extension AvPlayerDrmIos: AVAssetResourceLoaderDelegate {
  func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForLoadingOfRequestedResource loadingRequest: AVAssetResourceLoadingRequest) -> Bool {
  print("📜", #function, "Resource loader in.")
    guard let dataRequest = loadingRequest.dataRequest else {return false}
    // 6: Request the Server Playback Context from OS
    // To obtain the license request (Server Playback Context or SPC in Apple's terms), we call
    // .streamingContentKeyRequestData(forApp:contentIdentifier:options:)
    // using the information we obtained earlier.
    //licenseURL = (loadingRequest.request.url?.absoluteString ?? "").replacingOccurrences(of: "skd", with: "https"
    guard
      let contentIdData = (loadingRequest.request.url?.host ?? "").data(using: String.Encoding.utf8),
      let spcData = try? loadingRequest.streamingContentKeyRequestData(forApp: certificate!, contentIdentifier: contentIdData, options: nil) else {
      loadingRequest.finishLoading(with: NSError(domain: "com.icapps.error", code: -3, userInfo: nil))
      print("🔑", #function, "Unable to read the SPC data.")
      return false
    }

    // 7: Request CKC
    // Send the license request to the license server. The encrypted license response (Content Key
    // Context or CKC in Apple's terms) will contain the content key and associated playback policies.
    self.requestContentKeyAndLeaseExpiryfromKeyServerModule(withRequestBytes: spcData) { (ckc, error) in
      if error == nil && ckc != nil {
        // The CKC is correctly returned and is now send to the `AVPlayer` instance so we
        // can continue to play the stream.
        dataRequest.respond(with: ckc!)
        loadingRequest.contentInformationRequest?.contentType = AVStreamingKeyDeliveryContentKeyType
        loadingRequest.finishLoading()
      } else {
        print("🔑", #function, "Unable to fetch the CKC.")
        loadingRequest.finishLoading(with: error)
      }
    }

    return true
  }
}

