var exec = require('cordova/exec');
var PLUGIN_NAME = 'AvPlayerDrmIos';
var AvPlayerDrmIos = function() {};

AvPlayerDrmIos.play = function(arg0, onSuccess, onError) {
  exec(onSuccess, onError, PLUGIN_NAME, "play", [arg0]);
};

module.exports = AvPlayerDrmIos;
