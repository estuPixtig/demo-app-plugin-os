import {Component, OnInit} from '@angular/core';
import {AVPlayerServiceService} from '../avplayer-service.service';
import { v4 as uuidv4 } from 'uuid';
import {KJUR} from 'jsrsasign';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage{
  token: any;

  constructor(
    private avplayerService: AVPlayerServiceService
  ) {

  }

  playVideo() {
    const token = this.getTokenPlayerFairplay();
    this.avplayerService.play(token).then(r => console.log('TEST',r));
  }

  getTokenPlayerFairplay(): string {
    // Time variables
    var tNow = Math.floor(Date.now() / 1000) - 5;
    var tEnd = Math.floor(Date.now() / 1000) + 20;
    var myuuid = uuidv4();
    // Header
    var oHeader = {
      "alg": "ES256",
      "typ": "JWT",
      "kid": "6280e496-aa85-4292-b270-d439643df98c"
    };
    // Payload
    const oPayload = {
      "iss" : "tigosports",
      "sub" : "tigosportsgt-testdrm-hls",//"tigosportsgt-testdrm-dash",
      "iat": tNow,
      "exp": tEnd,
      "jti": myuuid,
      "subscriber":"tigosportsgt-testdrm-hls", // "tigosportsgt-testdrm-dash",
      "aud": "urn:verimatrix:multidrm",
      "nbf": tNow,
    };

    // Sign JWT
    var sHeader = JSON.stringify(oHeader);
    var sPayload = JSON.stringify(oPayload);

    var prvKey = "-----BEGIN EC PRIVATE KEY-----MHcCAQEEIKdhqIi53OA8L1uMlw2wZS45tlRrSABmRLu+zDL99AlLoAoGCCqGSM49AwEHoUQDQgAEYm9NGooxZZnF4VeFQqN+zHPfQnHlDt5ijqsEjOl9HiGq1yLebFJT5g3dis2AmdYnqtgcwM2Bnm/Er35bocym3Q==-----END EC PRIVATE KEY-----";
    //var prvKey = "-----BEGIN EC PRIVATE KEY-----MHcCAQEEICfDa0UwyG8QWxhIiuQqjJ2FiUXGO2d/YlbFsPhmtrAroAoGCCqGSM49AwEHoUQDQgAEYlj6Yl1fojK7cT1mxF+zfB9TagFpBDaEPQ7MxfMhRDLIRDckWpCqooWC72CGifn9Xs8N+uiJURyjdNzb67gCIQ==-----END EC PRIVATE KEY-----";
    var sJWT = KJUR.jws.JWS.sign("ES256", sHeader, sPayload, prvKey);

    return sJWT;
  }
}
