import { Component } from '@angular/core';
import {AVPlayerServiceService} from './avplayer-service.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor() {
  }
}
