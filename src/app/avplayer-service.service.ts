import { Injectable } from '@angular/core';
import { cordova, IonicNativePlugin } from '@ionic-native/core';


@Injectable({
  providedIn: 'root'
})

export class AVPlayerServiceService extends IonicNativePlugin {
  //name in package.json file of plugin
  static pluginName = 'AvPlayerDrmIos';
  // plugin id in the plugin.xml of plugin
  static plugin = 'cordova-plugin-avplayer-drm-ios';
  // clobbers target in the plugin.xml of plugin
  static pluginRef = 'AvPlayerDrmIos';
  static platforms = ['iOS'];

  play(token) {
    const data = cordova(this, 'play',{}, [
      {
        url: 'myschemehttps://dl4e2b546h0yx.cloudfront.net/out/v1/da9e5554cb31470792a9f1b8d4657d8c/index.m3u8',
        token
      }]);
    return data;
  }
}
